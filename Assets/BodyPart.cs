using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(IBodyPart))]
[RequireComponent(typeof(Rigidbody))]
public class BodyPart : MonoBehaviour, IBodyPart
{
    private Rigidbody rb;
    public void PushBody(Vector3 force, Vector3 pushPosition)
    {
        rb.AddForceAtPosition(force, pushPosition);
    }

    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
