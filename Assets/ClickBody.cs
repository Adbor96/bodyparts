using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClickBody : MonoBehaviour
{
    private Ray clickRay;
    private RaycastHit hit;
    [SerializeField]
    private float force;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            clickRay = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(clickRay, out hit))
            {
                //Debug.Log("Ray hit");
                Transform hitObject = hit.transform;
                IBodyPart hitBody = hitObject.GetComponent<IBodyPart>();
                if (hitBody != null)
                {
                    Debug.Log("Hit bodypart");
                    Vector3 direction = clickRay.direction * force;
                    hitBody.PushBody(direction, hit.point);
                }
                if (hitObject != null)
                {
                    //Debug.Log("Hit " + hitObject.name);
                }
            }
        }
    }
}
