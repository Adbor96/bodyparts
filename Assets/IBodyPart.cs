using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IBodyPart 
{
    void PushBody(Vector3 force, Vector3 pushPosition);
}
